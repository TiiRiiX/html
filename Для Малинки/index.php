﻿<html>
	<head>
		<title> Web Server on RPi | Home </title>
		<link rel="stylesheet" href="css/style.css" type="text/css"> 
		<script src="js/js.js"></script>
	</head>
	<body background="img/bg.png">
		<div id="main">
			<div id="dvig">
				<a href="/drupal">Drupal</a><br>
				<a href="/ffcms">FFcms</a><br>
				<a href="/wordpress">Wordpress</a><br>
				<a href="/modx">ModX</a><br>
			</div>
			<img src="img/Raspi.png" align=left id="logo">
			<p id="logon">
			Web <span>Server</span> on <span>Raspberry Pi</span>
			<br><span style="font-size:0.6em; color:black;">Специальная страница для навигации по веб серверу</span>
			</p>
			<hr>
			<img src="img/foto.jpg" align=left hspace=20 width=300 height=225>
			<span id="doc_time"> Дата и время </span><br><br>
			Эта страница создана специально для того, чтобы упростить навигацию по малинке!
			<br><br>Доступные сервисы:
			<br><a href="/random">Сайт Random</a>
			<br><a href="/buddy">Управление базами данных</a>
			<br><a href="/wordpress"><b>Блог разработки сервера</b></a>
			<script type="text/javascript">
			clock();
			</script>
			<br><br><hr>
			<?php
			function linuxUptime(){
				$ut = strtok(exec( "cat /proc/uptime" ), ".");
				$days = sprintf( "%2d", ($ut/(3600*24)) );
				$hours = sprintf( "%2d", ( ($ut % (3600*24)) / 3600) );
				$min = sprintf( "%2d", ($ut % (3600*24) % 3600)/60  );
				$sec = sprintf( "%2d", ($ut % (3600*24) % 3600)%60  );
				return array( $days, $hours, $min, $sec );
			}
			$ut = linuxUptime();
			echo "<div id='uptime'>Сервер в работе: $ut[0] дней, $ut[1] часов, $ut[2] минут, $ut[3] секунд</div>";
			
			$bytes = disk_free_space("."); 
			$si_prefix = array( 'B', 'KB', 'MB', 'GB', 'TB', 'EB', 'ZB', 'YB' );
			$base = 1024;
			$class = min((int)log($bytes , $base) , count($si_prefix) - 1);
			echo '<div id="memory"> Состояние доступной памяти: ';
			echo sprintf('%1.2f' , $bytes / pow($base,$class)) . ' ' . $si_prefix[$class] . ' / ';
			$bytes = disk_total_space("."); 
			$si_prefix = array( 'B', 'KB', 'MB', 'GB', 'TB', 'EB', 'ZB', 'YB' );
			$base = 1024;
			$class = min((int)log($bytes , $base) , count($si_prefix) - 1);
			echo sprintf('%1.2f' , $bytes / pow($base,$class)) . ' ' . $si_prefix[$class] . '</div>';

			?>
			<br>
		</div>
		<div id="bottom">
		Created by TiiRiiX | Raspberry Pi
		</div>
	</body>
</html>
